/* Copyright (C) 2012-2013 Alexandr Akulich
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.

 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:

 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "CExtendedArray.hpp"
#include <QTextCodec>

CExtendedArray::CExtendedArray() :
    QByteArray(),
    m_currentCodec(0)
{
    reset();
    setTextCodec("UTF-8");
}

CExtendedArray::CExtendedArray(const QByteArray &array) :
    QByteArray(),
    m_currentCodec(0)
{
    reset();
    setTextCodec("UTF-8");
    append(array);
}

quint8 CExtendedArray::readQUInt8()
{
    ++m_position;

    return at(m_position - 1);
}

quint16 CExtendedArray::readQUInt16()
{
    quint16 result = * (quint16 *) mid(m_position, 2).constData();
    m_position += 2;
    return result;
}

quint32 CExtendedArray::readQUInt32()
{
    quint32 result = * (quint32 *) mid(m_position, 4).constData();
    m_position += 4;
    return result;
}

qint8 CExtendedArray::readQInt8()
{
    ++m_position;

    return at(m_position - 1);
}

qint16 CExtendedArray::readQInt16()
{
    qint16 result = * (qint16 *) mid(m_position, 2).constData();
    m_position += 2;
    return result;
}

qint32 CExtendedArray::readQInt32()
{
    qint32 result = * (qint32 *) mid(m_position, 4).constData();
    m_position += 4;
    return result;
}

qreal CExtendedArray::readQReal()
{
    qreal result = * (qreal *) mid(m_position, 8).constData();
    m_position += 8;
    return result;
}

QByteArray CExtendedArray::readBytes(int count)
{
    m_position += count;
    return mid(m_position - count, count);
}

QByteArray CExtendedArray::readBlock()
{
    quint32 size = * (quint32 *) mid(m_position, 4).constData();
    m_position += size + 4;
    return mid(m_position - size, size);
}

QString CExtendedArray::readQString()
{
    return m_currentCodec->toUnicode(readBlock());
}

void CExtendedArray::setTextCodec(const QString &codecName)
{
    m_currentCodec = QTextCodec::codecForName(codecName.toLocal8Bit());
}

void CExtendedArray::readNamedBlock(QString &fileName, QByteArray &fileData)
{
    fileName = readBlock();
    fileData = readBlock();
}

void CExtendedArray::writeQUInt8(const quint8 value)
{
    append(value);
}

void CExtendedArray::writeQUInt16(const quint16 value)
{
    append((const char *) &value, 2);
}

void CExtendedArray::writeQUInt32(const quint32 value)
{
    append((const char *) &value, 4);
}

void CExtendedArray::writeQInt8(const qint8 value)
{
    append(value);
}

void CExtendedArray::writeQInt16(const qint16 value)
{
    append((const char *) &value, 2);
}

void CExtendedArray::writeQInt32(const qint32 value)
{
    append((const char *) &value, 4);
}

void CExtendedArray::writeQReal(const qreal &value)
{
    append((const char *) &value, 8);
}

void CExtendedArray::writeQString(const QString &value)
{
    writeQUInt32(value.size());
    append(value);
}

void CExtendedArray::writeBlock(const QByteArray &block)
{
    return writeBlock(&block);
}

void CExtendedArray::writeBlock(const QByteArray *pBlock)
{
    writeQUInt32(pBlock->size());
    append(pBlock->constData(), pBlock->size());
}

void CExtendedArray::writeBytes(const char *pBytes, int count)
{
    append(pBytes, count);
}

void CExtendedArray::writeNamedBlock(const QString &fileName, const QByteArray &fileData)
{
    writeQString(fileName);
    writeBlock(fileData);
}

bool CExtendedArray::atEnd() const
{
    if (size() <= m_position)
        return true;
    else
        return false;
}

void CExtendedArray::reset()
{
    clear();
    m_position = 0;
}

int CExtendedArray::bytesRemain()
{
    return size() - m_position;
}

int CExtendedArray::position() const
{
    return m_position;
}

void CExtendedArray::toBegin()
{
    m_position = 0;
}
