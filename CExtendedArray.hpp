/* Copyright (C) 2012-2013 Alexandr Akulich
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.

 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:

 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#ifndef CEXTENDEDARRAY_H
#define CEXTENDEDARRAY_H

#include <QByteArray>
#include <QString>

class QTextCodec;

class CExtendedArray : public QByteArray
{
public:
    CExtendedArray();
    CExtendedArray(const QByteArray &array);
    quint8  readQUInt8 ();
    quint16 readQUInt16();
    quint32 readQUInt32();
    qint8   readQInt8();
    qint16  readQInt16();
    qint32  readQInt32();
    qreal   readQReal();
    QByteArray readBytes(int count);
    QByteArray readBlock();
    QString    readQString();
    void setTextCodec(const QString &codecName);
    void readNamedBlock(QString &fileName, QByteArray &fileData);

    void writeQUInt8(const quint8 value);
    void writeQUInt16(const quint16 value);
    void writeQUInt32(const quint32 value);
    void writeQInt8(const qint8 value);
    void writeQInt16(const qint16 value);
    void writeQInt32(const qint32 value);
    void writeQReal(const qreal &value);
    void writeQString(const QString &value);
    void writeBlock(const QByteArray &block);
    void writeBlock(const QByteArray *pBlock);
    void writeBytes(const char *pBytes, int count);
    void writeNamedBlock(const QString &fileName, const QByteArray &fileData);
    bool atEnd() const;

    void reset();
    int bytesRemain();
    int position() const;
    void toBegin();

private:
    int m_position;
    QTextCodec *m_currentCodec;
};

#endif // CEXTENDEDARRAY_H
