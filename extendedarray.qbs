import qbs 1.0

Product {
    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core"] }
    name: "extendedarray"
    type: "dynamiclibrary"

    Group {
        name: "files"
        files: [
            "CExtendedArray.cpp",
            "CExtendedArray.hpp"
        ]
    }
}
